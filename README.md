# Model Training Method Analysis

## Description
The purpose of this repository is to utilize the CIFAR10 Dataset (http://www.cs.toronto.edu/~kriz/cifar.html, Learning Multiple Layers of Features from Tiny Images, Alex Krizhevsky, 2009) to look at several different methods for when to determine to retrain an existing model with new data. 

## Retraining Methodologies
Below are the Six determinating factors on when to retrain & redeploy a model.
* Short Timeframe - Retrain the model every 15 minutes
* Long Timeframe - Retrain the model every 2.5 hours
* Small New Dataset - Retrain the model every 10 Images
* Large New Dataset - Retrain the model every 100 Images
* Very Large New Dataset - Retrain the model every 500 Images
* Model Drift - Retrain the model when model drift based of accuracy is detected

## Quantifing Model Effectiveness
The following information will be saved through each of the above retraining methodologies for analysis. The data will be saved in each retraining cycle and as a whole training run.
* True/False Positives
* True/False Negatives
* Model Confidence average

## Starting Base Model
The starting base model for training will be utilizing the ResNet18 to begin with though, this repository is written in a way that the model is largely a module and may be switched out.